//
//  TicketsCell.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDTicketsCell.h"

@interface ZNDTicketsCell()

@property (weak, nonatomic) IBOutlet UILabel *descriptioLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;

@end

@implementation ZNDTicketsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - getter/setter

- (void)setViewModel:(ZNDTicketItemViewModel *)viewModel {

    self.descriptioLabel.text = viewModel.ticketDescription;
    self.numberLabel.text = viewModel.ticketNumber;
    self.statusLabel.text =  viewModel.ticketStatus;
    self.subjectLabel.text = viewModel.ticketSubject;
    self.statusLabel.textColor = viewModel.ticketCellColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
