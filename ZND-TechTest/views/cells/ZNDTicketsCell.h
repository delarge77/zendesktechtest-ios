//
//  TicketsCell.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNDTicket.h"
#import "ZNDTicketItemViewModel.h"

@interface ZNDTicketsCell : UITableViewCell

@property (nonatomic, strong) ZNDTicketItemViewModel *viewModel;


@end
