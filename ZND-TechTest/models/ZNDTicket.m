//
//  ZNDTicket.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDTicket.h"
#import <Mantle/NSValueTransformer+MTLPredefinedTransformerAdditions.h>

@implementation ZNDTicket

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ticketSubject" : @"subject",
             @"ticketDescription" : @"description",
             @"ticketNumber" : @"id",
             @"ticketStatus" : @"status"};
    
}

+ (NSValueTransformer *)ticketStatusJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"new" : @(ZNDTicketStatusNew),
                                                                           @"open" : @(ZNDTicketStatusOpen),
                                                                           @"pending" : @(ZNDTicketStatusPending),
                                                                           @"hold" : @(ZNDTicketStatusHold),
                                                                           @"solved": @(ZNDTicketStatusSolved),
                                                                           @"closed": @(ZNDTicketStatusClosed)}];
}

@end

