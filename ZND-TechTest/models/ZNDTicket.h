//
//  ZNDTicket.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSUInteger, ZNDTicketStatus) {
    ZNDTicketStatusNew,
    ZNDTicketStatusOpen,
    ZNDTicketStatusPending,
    ZNDTicketStatusHold,
    ZNDTicketStatusSolved,
    ZNDTicketStatusClosed,
    ZNDTicketStatusUndefined
};

@interface ZNDTicket : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *ticketSubject;
@property (nonatomic, copy, readonly) NSString *ticketDescription;
@property (nonatomic, copy, readonly) NSNumber *ticketNumber;
@property (nonatomic, readonly) ZNDTicketStatus ticketStatus;

@end
