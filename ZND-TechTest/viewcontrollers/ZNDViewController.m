//
//  ViewController.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDViewController.h"
#import "ZNDTechTestController.h"
#import "ZNDTicket.h"
#import "ZNDTicketsCell.h"
#import "ZNDTicketItemViewModel.h"

@interface ZNDViewController ()

@property (weak, nonatomic) IBOutlet UITableView *ticketsTableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSArray *ticketsViewModels;
@property (strong, nonatomic) ZNDTicketItemViewModel *ticketCellViewModel;

@end

@implementation ZNDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRefreshControl];
    [self loadTickets];
}

#pragma mark - Private Methods

- (void)addRefreshControl {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(loadTickets) forControlEvents:UIControlEventValueChanged];
    
    [self.ticketsTableView addSubview:self.refreshControl];
}

- (void)loadTickets {
    
    __weak __typeof(self) weakSelf = self;
    
    [ZNDTechTestController loadTicketsViewModels:^(NSArray *array, NSError *error) {
        [weakSelf.refreshControl endRefreshing];
        
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        } else {
            weakSelf.ticketsViewModels = array;
            [weakSelf.ticketsTableView reloadData];
        }
    }];
}

#pragma mark - UITableView Datasource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *ticketCell = @"ticketCell";
    
    ZNDTicketsCell *cell = [tableView dequeueReusableCellWithIdentifier:ticketCell];
    
    if (!cell) {
        cell = [[ZNDTicketsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ticketCell];
    }
    
    [cell setViewModel:self.ticketsViewModels[indexPath.row]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.ticketsViewModels.count;
}

@end