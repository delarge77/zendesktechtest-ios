//
//  ViewController.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZNDViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end

