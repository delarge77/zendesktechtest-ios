//
//  ZNDTechTestClient.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDTechTestConnectionClient.h"
#import "AFNetworkActivityIndicatorManager.h"

NSString *ZNDTechTestClientURL = @"https://mxtechtest.zendesk.com/";

@implementation ZNDTechTestConnectionClient

+ (instancetype)sharedClient {
    static ZNDTechTestConnectionClient * _sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[ZNDTechTestConnectionClient alloc] initWithBaseURL:[NSURL URLWithString:ZNDTechTestClientURL]];
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    });
    
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        self.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.requestSerializer setAuthorizationHeaderFieldWithUsername:@"acooke+techtest@zendesk.com" password:@"mobile"];
    }
    
    return self;
}

@end
