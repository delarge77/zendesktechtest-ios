//
//  ZNDTechTestClient.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "AFHTTPSessionManager.h"

extern NSString *ZNDTechTestClientURL;

@interface ZNDTechTestConnectionClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
