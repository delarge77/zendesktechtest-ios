//
//  ZNDTechTestController.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDTechTestController.h"
#import "ZNDTechTestConnectionClient.h"
#import "MTLJSONAdapter.h"
#import "ZNDTicket.h"
#import "ZNDTicketItemViewModel.h"

NSString *ZNDTechTestControllerErrorDomain = @"com.zendesk:ZNDTechTestControllerErrorDomain";

@implementation ZNDTechTestController

+ (NSURLSessionTask *)loadTicketsWithCompletionHandler:(ZNDTechTestCompletionHandler)completionHandler {

    [[[ZNDTechTestConnectionClient sharedClient] operationQueue] cancelAllOperations];
    
    return [[ZNDTechTestConnectionClient sharedClient] GET:@"api/v2/views/39551161/tickets.json" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error = nil;
        
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        NSArray *result = [MTLJSONAdapter modelsOfClass:[ZNDTicket class]
                                          fromJSONArray:jsonResponse[@"tickets"]
                                                  error:&error];
        if (completionHandler) completionHandler(result, nil);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if ([error.domain isEqualToString:NSURLErrorDomain]) {
            
            error = [self buildConnectionProblemError];
            if (completionHandler) completionHandler(nil, error);
            return;
        }
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        if (response.statusCode >= 400 && response.statusCode <= 499) {
            error = [self build400Error];
        }
        if (response.statusCode >= 500 && response.statusCode <= 599) {
            error = [self build500Error];
        }
        
        if (completionHandler) completionHandler(nil, error);

    }];
}

+ (void)loadTicketsViewModels:(ZNDTechTestCompletionHandler)completionHandler {
    [ZNDTechTestController loadTicketsWithCompletionHandler:^(NSArray *array, NSError *error) {
        if (error) {
            NSLog(@"%@", error.localizedDescription);
        } else {
            
            NSMutableArray *viewModels = [NSMutableArray array];
            
            for (ZNDTicket *ticket in array) {
                
                ZNDTicketItemViewModel *viewModel = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticket];
                
                if (viewModel) {
                    [viewModels addObject:viewModel];
                }
            }
            
            if (completionHandler) {
                completionHandler([viewModels copy], nil);
            }
        }
    }];
}

+ (NSError *)buildConnectionProblemError {
    NSDictionary *userInfo =
    @{NSLocalizedDescriptionKey : NSLocalizedString(@"ZNDTechTestController.buildConnectionProblemError", nil)};
    
    return [NSError errorWithDomain:ZNDTechTestControllerErrorDomain
                               code:ZNDTechTestControllerErrorCodeConnectionProblem
                           userInfo:userInfo];
}

+ (NSError *)build400Error {
    NSDictionary *userInfo =
    @{NSLocalizedDescriptionKey : NSLocalizedString(@"ZNDTechTestController.build400Error", nil)};
    
    return [NSError errorWithDomain:ZNDTechTestControllerErrorDomain
                               code:ZNDTechTestControllerErrorCodeBadRequest
                           userInfo:userInfo];
}

+ (NSError *)build500Error {
    NSDictionary *userInfo =
    @{NSLocalizedDescriptionKey :
          NSLocalizedString(@"ZNDTechTestController.build500Error", nil)};
    
    return [NSError errorWithDomain:ZNDTechTestControllerErrorDomain
                               code:ZNDTechTestControllerErrorCodeServerError
                           userInfo:userInfo];
}

@end
