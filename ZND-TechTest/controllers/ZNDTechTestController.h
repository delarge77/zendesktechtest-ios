//
//  ZNDTechTestController.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/28/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *ZNDTechTestControllerErrorDomain;

typedef void (^ZNDTechTestCompletionHandler)(NSArray *array, NSError *error);

typedef NS_ENUM(NSUInteger, ZNDTechTestControllerErrorCode) {
    ZNDTechTestControllerErrorCodeConnectionProblem,
    ZNDTechTestControllerErrorCodeBadRequest,
    ZNDTechTestControllerErrorCodeServerError,
};

@interface ZNDTechTestController : NSObject

+ (NSURLSessionTask *)loadTicketsWithCompletionHandler:(ZNDTechTestCompletionHandler)completionHandler;
+ (void)loadTicketsViewModels:(ZNDTechTestCompletionHandler)completionHandler;

@end
