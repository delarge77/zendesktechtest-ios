//
//  ZNDTicketItemViewModel.h
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/29/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNDTicket.h"
#import <UIKit/UIKit.h>

@interface ZNDTicketItemViewModel : NSObject

@property (nonatomic, copy, readonly) NSString *ticketDescription;
@property (nonatomic, copy, readonly) NSString *ticketNumber;
@property (nonatomic, copy, readonly) NSString *ticketStatus;
@property (nonatomic, copy, readonly) NSString *ticketSubject;
@property (nonatomic, copy, readonly) UIColor *ticketCellColor;

- (instancetype)initWithTicketItem:(ZNDTicket *) ticket;

@end
