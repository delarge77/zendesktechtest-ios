//
//  ZNDTicketItemViewModel.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/29/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import "ZNDTicketItemViewModel.h"

@interface ZNDTicketItemViewModel()

@property (nonatomic, strong) ZNDTicket *ticket;

@end

@implementation ZNDTicketItemViewModel

- (instancetype)initWithTicketItem:(ZNDTicket *)ticket {

    NSParameterAssert(ticket);
    self = [super init];
    if (self) {
        self.ticket = ticket;
        
    }
    return self;
}

#pragma mark - getter/setter

- (void)setTicket:(ZNDTicket *) ticket {
    _ticket = ticket;
    
    _ticketDescription = ticket.ticketDescription;
    _ticketNumber = [ticket.ticketNumber stringValue];
    _ticketStatus =  [self setTicketStatus:ticket.ticketStatus];
    _ticketSubject = ticket.ticketSubject;
    _ticketCellColor = [self setCellColorForStatus:ticket.ticketStatus];
}

- (NSString *)setTicketStatus:(ZNDTicketStatus)ticketStatus {
    
    NSString *status = nil;
    
    switch (ticketStatus) {
        case ZNDTicketStatusNew:
            status = NSLocalizedString(@"ZNDTicketStatus.new", nil);
            break;
        case ZNDTicketStatusOpen:
            status = NSLocalizedString(@"ZNDTicketStatus.open", nil);
            break;
        case ZNDTicketStatusPending:
            status = NSLocalizedString(@"ZNDTicketStatus.pending", nil);
            break;
        case ZNDTicketStatusHold:
            status = NSLocalizedString(@"ZNDTicketStatus.hold", nil);
            break;
        case ZNDTicketStatusSolved:
            status = NSLocalizedString(@"ZNDTicketStatus.solved", nil);
            break;
        case ZNDTicketStatusClosed:
            status = NSLocalizedString(@"ZNDTicketStatus.closed", nil);
            break;
        default:
            status = NSLocalizedString(@"ZNDTicketStatus.undefined", nil);
            break;
    }
    
    return status;
}

- (UIColor *) setCellColorForStatus:(ZNDTicketStatus) status {
    
    UIColor *cellColor = nil;
    
    switch (status) {
        case ZNDTicketStatusNew:
            cellColor = [UIColor blueColor];
            break;
        case ZNDTicketStatusOpen:
            cellColor = [UIColor greenColor];
            break;
        case ZNDTicketStatusPending:
            cellColor = [UIColor redColor];
            break;
        case ZNDTicketStatusHold:
            cellColor = [UIColor grayColor];
            break;
        case ZNDTicketStatusSolved:
            cellColor = [UIColor blackColor];
            break;
        case ZNDTicketStatusClosed:
            cellColor = [UIColor blackColor];
            break;
        default:
            cellColor = [UIColor yellowColor];
            break;
    }
    
    return cellColor;
}

@end
