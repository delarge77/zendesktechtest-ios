//
//  ZNDTechTestControllerTests.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/29/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "OHHTTPStubs.h"
#import "ZNDTechTestController.h"
#import "ZNDTicket.h"
#import <OHHTTPStubs/OHHTTPStubsResponse+JSON.h>
#import <OHHTTPStubs/OHPathHelpers.h>

@interface ZNDTechTestControllerTests: XCTestCase

@end

@implementation ZNDTechTestControllerTests

- (void)tearDown {
    [OHHTTPStubs removeAllStubs];
}

- (void)testShouldReturnBadRequestWhen4xxStatusCodeIsReturned {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString rangeOfString:@"/tickets.json"].location != NSNotFound;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:400 headers:nil];
    }];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Should return invalid error request for error 4xx"];
    NSURLSessionTask *task = [ZNDTechTestController loadTicketsWithCompletionHandler:^(NSArray *array, NSError *error) {
        XCTAssertNil(array);
        XCTAssertNotNil(error);
        XCTAssertEqual(error.code, ZNDTechTestControllerErrorCodeBadRequest);
        XCTAssertEqualObjects(error.domain, ZNDTechTestControllerErrorDomain);
        XCTAssertEqualObjects(error.userInfo[NSLocalizedDescriptionKey], NSLocalizedString(@"ZNDTechTestController.build400Error", nil));
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        [task cancel];
    }];
}

- (void)testShouldReturnServerErrorWhen5xxStatusCodeIsReturned {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString rangeOfString:@"/tickets.json"].location != NSNotFound;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithData:[NSData data] statusCode:500 headers:nil];
    }];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Should return invalid error request for error 5xx"];
    NSURLSessionTask *task = [ZNDTechTestController loadTicketsWithCompletionHandler:^(NSArray *array, NSError *error) {
        XCTAssertNil(array);
        XCTAssertNotNil(error);
        XCTAssertEqual(error.code, ZNDTechTestControllerErrorCodeServerError);
        XCTAssertEqualObjects(error.domain, ZNDTechTestControllerErrorDomain);
        XCTAssertEqualObjects(error.userInfo[NSLocalizedDescriptionKey], NSLocalizedString(@"ZNDTechTestController.build500Error", nil));
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        [task cancel];
    }];
    
}

- (void)testShouldReturnConnectionErrorWhenURLErrorDomainIsReturned {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString rangeOfString:@"/tickets.json"].location != NSNotFound;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        NSError *notConnectedError = [NSError errorWithDomain:NSURLErrorDomain
                                                         code:kCFURLErrorNotConnectedToInternet
                                                     userInfo:nil];
        return [OHHTTPStubsResponse responseWithError:notConnectedError];
    }];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Should return connection error when not connected"];
    NSURLSessionTask *task = [ZNDTechTestController loadTicketsWithCompletionHandler:^(NSArray *array, NSError *error) {
        XCTAssertNil(array);
        XCTAssertNotNil(error);
        XCTAssertEqual(error.code, ZNDTechTestControllerErrorCodeConnectionProblem);
        XCTAssertEqualObjects(error.domain, ZNDTechTestControllerErrorDomain);
        XCTAssertEqualObjects(error.userInfo[NSLocalizedDescriptionKey], NSLocalizedString(@"ZNDTechTestController.buildConnectionProblemError", nil));
        
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        [task cancel];
    }];
    
}

- (void)testShouldBeAbleToParseAndReturnTickets {
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
        return [request.URL.absoluteString rangeOfString:@"/tickets.json"].location != NSNotFound;
    } withStubResponse:^OHHTTPStubsResponse*(NSURLRequest *request) {
        return [OHHTTPStubsResponse responseWithFileAtPath:OHPathForFile(@"/tickets.json",self.class)
                                                statusCode:200 headers:@{@"Content-Type":@"application/json"}];
    }];
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Should be able to parse and return tickets"];
    NSURLSessionTask *task = [ZNDTechTestController loadTicketsWithCompletionHandler:^(NSArray *array, NSError *error) {
        XCTAssertNil(error, @"Should be able to parse tickets without an error");
        XCTAssertEqual(45, array.count, @"Should be able to parse all 45 tickets inside the fixture");
        [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            XCTAssertTrue([obj isKindOfClass:[ZNDTicket class]], @"Should have only ZNDTicket objects inside response");
        }];
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        [task cancel];
    }];
}

@end
