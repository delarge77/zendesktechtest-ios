//
//  ZNDTicketItemViewModelTests.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/30/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZNDTicket.h"
#import "ZNDTicketItemViewModel.h"
#import "ZNDModelConverter.h"

@interface ZNDTicketItemViewModelTests : XCTestCase

@end

@implementation ZNDTicketItemViewModelTests

- (void)testShouldFailIfNilModelIsSent {
    
    XCTAssertThrows([[ZNDTicketItemViewModel alloc] initWithTicketItem:nil], @"Should fail when sending nil as parameter");
}

- (void) testShouldSetRightStatus {

    NSDictionary *valuesOne = @{@"subject" : @"Ticket : 12",
                                @"description": @"This is a test ticket",
                                @"status":@"new",
                                @"id":@101};
    NSDictionary *valuesTwo = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"open",
                                @"id":@102};
    NSDictionary *valuesThree = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"pending",
                                @"id":@103};
    NSDictionary *valuesFour = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"hold",
                                @"id":@104};
    NSDictionary *valuesFive = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"solved",
                                @"id":@105};
    NSDictionary *valuesSix = @{@"subject" : @"Ticket : 99",
                                 @"description": @"This is a test ticket",
                                 @"status":@"closed",
                                 @"id":@106};
    
    ZNDTicket *ticketOne = [ZNDModelConverter convertModelFromJSON:valuesOne class:[ZNDTicket class]];
    ZNDTicket *ticketTwo = [ZNDModelConverter convertModelFromJSON:valuesTwo class:[ZNDTicket class]];
    ZNDTicket *ticketThree = [ZNDModelConverter convertModelFromJSON:valuesThree class:[ZNDTicket class]];
    ZNDTicket *ticketFour = [ZNDModelConverter convertModelFromJSON:valuesFour class:[ZNDTicket class]];
    ZNDTicket *ticketFive = [ZNDModelConverter convertModelFromJSON:valuesFive class:[ZNDTicket class]];
    ZNDTicket *ticketSix = [ZNDModelConverter convertModelFromJSON:valuesSix class:[ZNDTicket class]];
    
    ZNDTicketItemViewModel *viewModelOne = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketOne];
    ZNDTicketItemViewModel *viewModelTwo = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketTwo];
    ZNDTicketItemViewModel *viewModelThree = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketThree];
    ZNDTicketItemViewModel *viewModelFour = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketFour];
    ZNDTicketItemViewModel *viewModelFive = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketFive];
    ZNDTicketItemViewModel *viewModelSix = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketSix];

    XCTAssertTrue([viewModelOne.ticketStatus isEqualToString:@"New"], @"Ticket status should be new");
    XCTAssertTrue([viewModelTwo.ticketStatus isEqualToString:@"Open"], @"Ticket status should be open");
    XCTAssertTrue([viewModelThree.ticketStatus isEqualToString:@"Pending"], @"Ticket status should be pending");
    XCTAssertTrue([viewModelFour.ticketStatus isEqualToString:@"Hold"], @"Ticket status should be hold");
    XCTAssertTrue([viewModelFive.ticketStatus isEqualToString:@"Solved"], @"Ticket status should be solved");
    XCTAssertTrue([viewModelSix.ticketStatus isEqualToString:@"Closed"], @"Ticket status should be closed");
    
}

- (void) testShoulCreateTicketViewModel {

    NSDictionary *valuesOne = @{@"subject" : @"Ticket : 12",
                                @"description": @"This is a test ticket",
                                @"status":@"new",
                                @"id":@101};
    
    ZNDTicket *ticketOne = [ZNDModelConverter convertModelFromJSON:valuesOne class:[ZNDTicket class]];
    ZNDTicketItemViewModel *viewModel = [[ZNDTicketItemViewModel alloc] initWithTicketItem:ticketOne];
    
    XCTAssertTrue([viewModel.ticketSubject isEqualToString:@"Ticket : 12"]);
    XCTAssertTrue([viewModel.ticketDescription isEqualToString:@"This is a test ticket"]);
    XCTAssertTrue([viewModel.ticketNumber isEqualToString:@"101"]);
    XCTAssertTrue([viewModel.ticketStatus isEqualToString:@"New"]);
    
}

@end
