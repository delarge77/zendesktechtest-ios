//
//  ZNDTicketTests.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/30/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZNDTicket.h"
#import "ZNDModelConverter.h"

@interface ZNDTicketTests : XCTestCase

@end

@implementation ZNDTicketTests

- (void)testShouldBeAbleToParseAValidTicketObject {
    
   NSDictionary *values = @{@"created_at": @"2014-01-30T14:18:40Z",
                            @"updated_at": @"2014-01-30T14:18:40Z",
                            @"type": @"problem",
                            @"subject": @"Ticket : 99",
                            @"raw_subject": @"Ticket : 99",
                            @"description": @"This is a test ticket",
                            @"priority": @"high",
                            @"status": @"new",
                            @"recipient": [NSNull null],
                            @"requester_id": @(465863061),
                            @"submitter_id": @(465863061),
                            @"assignee_id": [NSNull null],
                            @"organization_id": @(28603191),
                            @"group_id": @(21750951),
                            @"collaborator_ids": @[],
                            @"forum_topic_id": [NSNull null],
                            @"problem_id": [NSNull null],
                            @"has_incidents": @YES,
                            @"is_public": @YES,
                            @"due_at": [NSNull null],
                            @"tags": @[],
                            @"custom_fields": @[],
                            @"satisfaction_rating": [NSNull null],
                            @"sharing_agreement_ids": @[],
                            @"fields": @[],
                            @"brand_id": @(85821),
                            @"allow_channelback": @NO,
                            @"url": @"https://mxtechtest.zendesk.com/api/v2/tickets/101.json",
                            @"id": @(101),
                            @"external_id": [NSNull null],
                            @"via": @{
                                @"channel": @"api",
                                @"source": @{
                                    @"from": @{},
                                    @"to": @{},
                                    @"rel": [NSNull null]}}};


    ZNDTicket *ticket = [ZNDModelConverter convertModelFromJSON:values class:[ZNDTicket class]];
    XCTAssertNotNil(ticket, @"Should not be nil when converting a valid ticket model");
    XCTAssertEqual(ticket.ticketStatus, ZNDTicketStatusNew, @"Should be an new state");
    XCTAssertEqualObjects(ticket.ticketNumber, @(101), @"Should be the same ticket id");
    XCTAssertEqualObjects(ticket.ticketSubject, @"Ticket : 99", @"Should be the same subject");
    XCTAssertEqualObjects(ticket.ticketDescription, @"This is a test ticket", @"Should be the same description");
}

- (void)testShouldBeSameTicketWhenIdentifierIsTheSame {

    NSDictionary *valuesOne = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"new",
                                @"id":@101};
    NSDictionary *valuesTwo = @{@"subject" : @"Ticket : 99",
                                @"description": @"This is a test ticket",
                                @"status":@"new",
                                @"id":@101};
    
    ZNDTicket *ticketOne = [ZNDModelConverter convertModelFromJSON:valuesOne class:[ZNDTicket class]];
    ZNDTicket *ticketTwo = [ZNDModelConverter convertModelFromJSON:valuesTwo class:[ZNDTicket class]];
    
    XCTAssertEqualObjects(ticketOne, ticketTwo, @"Should be same ticket just by compering ticket id");
}

@end
