//
//  ZNDModelConverterTests.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/29/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZNDModelConverter.h"
#import "ZNDTicket.h"

@interface ZNDDummyClass : NSObject

@end

@implementation ZNDDummyClass

@end

@interface ZNDModelConverterTests : XCTestCase

@end

@implementation ZNDModelConverterTests

- (void)testShouldFailIfNilClassToParseIsUsedForDictionaries {
    XCTAssertThrows([ZNDModelConverter convertModelFromJSON:@{} class:nil], @"Should fail when sending a nil class to convert an dictionary");
}

- (void)testShouldFailIfNilClassToParseIsUsedForArrays {
    XCTAssertThrows([ZNDModelConverter convertModelsFromJSON:@[] class:nil], @"Should fail when sending a nil class to convert an array");
}

- (void)testShouldReturnNilWhenSendingNilDictionaryToConvert {
    XCTAssertNil([ZNDModelConverter convertModelFromJSON:nil class:[ZNDTicket class]], @"Should return nil when converting nil dictionary");
}

- (void)testShouldReturnNilWhenSendingNilArrayToConvert {
    XCTAssertNil([ZNDModelConverter convertModelsFromJSON:nil class:[ZNDTicket class]], @"Should return nil when converting nil array");
}

- (void)testShouldReturnEmptyArrayIfEmptyArrayIsSent {
    NSArray *convertedObject = [ZNDModelConverter convertModelsFromJSON:@[] class:[ZNDTicket class]];
    XCTAssertNotNil(convertedObject, @"Should not be nil when converting empty array");
    XCTAssertTrue(0 == convertedObject.count, @"");
}

- (void)testShouldReturnNilIfInvalidModelClassIsSent {
    XCTAssertNil([ZNDModelConverter convertModelFromJSON:@{} class:[ZNDDummyClass class]], @"Should return nil if invalid model class is sent");
    XCTAssertNil([ZNDModelConverter convertModelsFromJSON:@[@{}] class:[ZNDDummyClass class]], @"Should return nil if invalid model class is sent");
}

@end