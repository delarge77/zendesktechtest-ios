//
//  ZNDTechTestConnectionClientTests.m
//  ZND-TechTest
//
//  Created by Alessandro dos santos pinto on 12/29/16.
//  Copyright © 2016 Alessandro dos Santos Pinto. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ZNDTechTestConnectionClient.h"

@interface ZNDTechTestConnectionClientTests : XCTestCase

@end

@implementation ZNDTechTestConnectionClientTests

- (void)testAsynchronousURLConnection {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Should be able to make requests"];
    NSString *path = @"api/v2/views/39551161/tickets.json";
    
    NSURLSessionDataTask *task = [[ZNDTechTestConnectionClient sharedClient] GET:path
                                                                      parameters:nil
                                                                        progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        XCTAssertNotNil(responseObject, "Should not have nil response");
        
        NSURLResponse *response = task.response;
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            XCTAssertEqual(200, httpResponse.statusCode, @"Should have 200 status code response");
            
            NSString *url = httpResponse.URL.absoluteString;
            BOOL containsURLClient = [url rangeOfString:ZNDTechTestClientURL].location != NSNotFound;
            XCTAssertTrue(containsURLClient, @"Should request to the right URL");
            
            BOOL containsPath = [url rangeOfString:path].location != NSNotFound;
            XCTAssertTrue(containsPath, @"Should contains path when requesting");
            
            XCTAssertEqualObjects(httpResponse.MIMEType, @"application/json", @"Should have right MIMEType when requesting");
        } else {
            XCTFail(@"Response was not NSHTTPURLResponse");
        }
        
        [expectation fulfill];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        XCTFail(@"Should not fail when requesting");
    }];
    
    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        [task cancel];
    }];
}

@end
